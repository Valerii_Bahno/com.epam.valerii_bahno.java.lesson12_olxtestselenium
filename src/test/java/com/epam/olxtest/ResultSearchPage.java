package com.epam.olxtest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultSearchPage extends BasePage {

    @FindBy(xpath = "//strong[contains(text(), 'Швейная машинка Подольск г.Никополь (рабочая)')]")
    private WebElement sewingMachine;

    @FindBy(xpath = "//strong[contains(text(), 'Собрание Владимира Набокова 4 тома')]")
    private WebElement collectionBooks;

    @FindBy(xpath = "//strong[text()='Игрушка электропоезд 70 см']")
    private WebElement toyElectricTrain;

    @FindBy(xpath = "//strong[contains(text(), 'Военная техника на реставрацию')]")
    private WebElement militaryMachines;

    public ResultSearchPage(WebDriver driver) {
        super(driver);
    }

    public void clickSewingMachine() {
        sewingMachine.click();
    }

    public void clickCollectionBooks() {
        collectionBooks.click();
    }

    public void clickToyElectricTrain() {
        toyElectricTrain.click();
    }

    public void clickMilitaryMachines() {
        militaryMachines.click();
    }
}
