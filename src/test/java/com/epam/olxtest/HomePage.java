package com.epam.olxtest;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@id='headerSearch']")
    private WebElement fieldSearch;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void search(String searchText) {
        fieldSearch.sendKeys(searchText, Keys.ENTER);
        new ResultSearchPage(driver);
    }
}
