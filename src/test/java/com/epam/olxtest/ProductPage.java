package com.epam.olxtest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage extends BasePage {

    @FindBy(xpath = "//div[@data-rel='phone']/strong[@class='xx-large']")
    private WebElement phoneUser;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickPhoneUser() {
        phoneUser.click();
        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[contains(text(), '0 500 674337')]")));
    }
}
