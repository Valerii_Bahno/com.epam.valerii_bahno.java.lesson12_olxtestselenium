import com.epam.olxtest.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestOlx {

    private WebDriver driver;
    private static final String olxUrl = "https://www.olx.ua/";
    private static HomePage homePage;
    private static ResultSearchPage resultSearchPage;
    private static ProductPage productPage;

    @Before
    public void start() {

        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(olxUrl);

        homePage = new HomePage(driver);
        productPage = new ProductPage(driver);
        resultSearchPage = new ResultSearchPage(driver);
    }

    @Test
    public void checkResultsOfSearchExistingGoods() {

        homePage.search("Xiaomi Redmi Note 8 Pro 6/64 gb");

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='offers_table']")));

        assertTrue(driver.findElement(By.xpath("//p[contains(text(), 'Найдено')]")).isDisplayed());
    }

    @Test
    public void checkResultsOfSearchNonExistentProduct() {

        homePage.search("Xiaomi Redmi Note 8 Pro 12/64 gb");

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='offers_table']")));

        assertTrue(driver.findElement(By.xpath("//p[contains(text(), 'Не найдено ни одного объявления')]")).isDisplayed());
    }

    @Test
    public void findGoodsAndCheckDescription() {

        homePage.search("Швейная машинка Подольск Никополь");

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='offers_table']")));

        resultSearchPage.clickSewingMachine();

        assertTrue(driver.findElement(By.id("textContent")).getText().contains("есть футляр"));
    }

    @Test
    public void findGoodsAndCheckPhoneSeller() {

        homePage.search("Собрание Владимира Набокова 4 тома");

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='offers_table']")));

        resultSearchPage.clickCollectionBooks();
        productPage.clickPhoneUser();

        WebElement showPhoneUser = driver.findElement(By.xpath("//ul[@id='contact_methods']//strong"));

        assertTrue(showPhoneUser.getText().contains("0 500 674337"));
    }

    @Test
    public void checkQuantityCategoryOnMainPage() {

        int quantityCategory = driver.findElements(By.xpath("//div[@class='item']")).size();

        assertEquals(13, quantityCategory);
    }

    @Test
    public void findGoodsAndCheckNameSeller() {

        homePage.search("Игрушка электропоезд 70 см");

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[contains(text(), 'Игрушка электропоезд 70 см')]")));

        resultSearchPage.clickToyElectricTrain();

        WebElement userName = driver.findElement(By.xpath("//a[contains(text(), 'Владимир Иванович')]"));

        assertTrue(userName.getText().contains("Владимир Иванович"));
    }

    @Test
    public void postNewAdAndMustRegistration() {

        driver.findElement(By.id("postNewAdLink")).click();

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//section/div[@class='login-box']")));

        assertTrue(driver.findElement(By.xpath("//section/div[@class='login-box']")).isDisplayed());
    }

    @Test
    public void findGoodsAndCheckPrice() {

        homePage.search("Военная техника на реставрацию");

        new WebDriverWait(driver, 10, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[contains(text(), 'Военная техника на реставрацию')]")));

        resultSearchPage.clickMilitaryMachines();

        WebElement priceProduct = driver.findElement(By.xpath("//div[@id='offerdescription']//div[@class='pricelabel']"));

        assertTrue(priceProduct.getText().contains("350 грн."));
    }

    @After
    public void close() {
        driver.quit();
    }
}
